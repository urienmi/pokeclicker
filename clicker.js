let clicker = setInterval(() => {
    const breeding = App.game.breeding;
    // Click on wild pokemons
    Battle.clickAttack();
    // Click on gym pokemons
    GymBattle.clickAttack();
    // For each egg slots available
    for (let i=0;i<breeding.eggSlots;i++) {
      // Get the egg
      const egg = breeding._eggList[i]();

      // If the egg is over, hatch it and set another random one to the hatchery
      if (egg.progress() === 100) {
        breeding.hatchPokemonEgg(i)
        const pokemon = PartyController.getHatcherySortedList()[Math.floor(Math.random() * PartyController.getHatcherySortedList().length)];
        breeding.addPokemonToHatchery(pokemon);
      }
    }
  }, 1)
  
  clearInterval(clicker)