*** Settings ***
Library         SeleniumLibrary
Suite Teardown  Teardown

*** Variables ***
${URL}=  https://www.pokeclicker.com/
${BROWSER}=  chrome


${title}=   Pokéclicker
${trainerCard}=  xpath\=/html/body/div[3]/div[1]/div[3]/div/div 

${startMenu}=  xpath\=/html/body/div[4]/button
${saveDropdownItem}=  xpath\=/html/body/div[4]/ul/li[6]/a
${downloadSaveBtn}=  xpath\=/html/body/div[36]/div/div/div[2]/div/div[1]/div/button[3]

${clickerFunction}=  setInterval(() =>{const breeding = App.game.breeding;Battle.clickAttack();GymBattle.clickAttack();for (let i=0;i<breeding.eggSlots;i++) {const egg = breeding._eggList[i]();egg.progress() === 100 && breeding.hatchPokemonEgg(i)}}, 1)
*** Test Cases ***

Open Browser 
    Open Browser  ${URL}  ${BROWSER}
    Maximize Browser Window

Import Save
    Wait Until Page Contains  Import Save
    Choose File  id:import-save  C:/Users/micka/Documents/Pokéclicker/save.txt
    Wait Until Element Is Visible  ${trainerCard}

Enter in a new world
    Click Element  ${trainerCard}
    Wait Until Page Contains  ${title}
    Activate Clicker
    FOR    ${index}    IN RANGE    99999
        # Run Keyword And Ignore Error  Hatch Eggs
        Sleep  5s
    END

Stop
    Desactivate Clicker
    Export Save

*** Keywords ***
Teardown
    Close All Browsers

Export Save
    Click Element   ${startMenu}
    Click Link    ${saveDropdownItem}
    Wait Until Element Is Visible  ${downloadSaveBtn}
    Click Element    ${downloadSaveBtn}

Activate Clicker
    Execute Javascript  ${clickerFunction}

Desactivate Clicker
    Execute Javascript  clearInterval(window.clicker);

Hatch Eggs
   Wait Until Page Contains  Hatch!
   Click Element  .progress-bar .bg-success